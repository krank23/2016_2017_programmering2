﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{

    GameObject[,] tiles;

    [SerializeField]
    protected int width = 3;

    [SerializeField]
    protected int height = 3;

    [SerializeField]
    GameObject tilePrefab;

    // Use this for initialization
    void Start()
    {
        tiles = new GameObject[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                PutTile(x, y);
            }
        }

    }

    protected virtual void PutTile(int x, int y)
    {
        tiles[x, y] = Instantiate(tilePrefab);

        Vector3 pos = new Vector3(x, y) + this.transform.position;

        tiles[x, y].transform.position = pos;

        tiles[x, y].transform.SetParent(this.transform);
    }
}
