﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGridGenerator : GridGenerator {

    protected override void PutTile(int x, int y)
    {

        if (x == 0 || y == 0 || 
            x == width-1 || y == height-1)
        {
            base.PutTile(x, y);
        }

        else if (x % 2 == 0 && y % 2 == 0)
        {
            base.PutTile(x, y);
        }

    }

}
