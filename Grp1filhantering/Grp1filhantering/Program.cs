﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Grp1filhantering
{
    class Program
    {
        static void Main(string[] args)
        {

            XmlSerializer spaceshipSerializer = new XmlSerializer(typeof(Spaceship));

            Spaceship ship;

            using (FileStream file = File.Open(@"spaceship.xml", FileMode.OpenOrCreate, FileAccess.Read))
            {
                ship = (Spaceship) spaceshipSerializer.Deserialize(file);
            }

            Console.WriteLine(ship.hp);

            /*Spaceship ship = new Spaceship();

            XmlSerializer spaceshipSerializer = new XmlSerializer(typeof(Spaceship));

            using(FileStream file = File.Open(@"spaceship.xml", FileMode.OpenOrCreate, FileAccess.Write))
            {
                spaceshipSerializer.Serialize(file, ship);
            }*/



            /*string s = "Hello, World!";
            File.WriteAllText(@"test.txt", s);*/

            /*string s = File.ReadAllText(@"test.txt");

            Console.WriteLine(s);*/


            /*string[] words = File.ReadAllLines(@"test.txt");

            words[1] = "Barbapappa";

            File.WriteAllLines(@"test.txt", words);*/

            Console.ReadLine();
        }
    }
}
