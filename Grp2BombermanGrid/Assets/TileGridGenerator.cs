﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileGridGenerator : MonoBehaviour
{

    [SerializeField]
    protected int width = 5;

    [SerializeField]
    protected int height = 5;

    [SerializeField]
    GameObject tilePrefab;

    GameObject[,] tiles;

    // Use this for initialization
    void Start()
    {

        tiles = new GameObject[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                MakeTile(x, y);
            }
        }

    }

    protected virtual void MakeTile(int x, int y)
    {
        tiles[x, y] = Instantiate(tilePrefab);
        Vector3 pos = new Vector3(x, y) + this.transform.position;
        tiles[x, y].transform.position = pos;

        tiles[x, y].transform.SetParent(this.transform);

    }

}
