﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGenerator : TileGridGenerator {

    protected override void MakeTile(int x, int y)
    {
        if (x == 0 || y == 0)
        {
            base.MakeTile(x, y);
        }
        else if (x == width-1 || y == height-1)
        {
            base.MakeTile(x, y);
        }
        else if (x % 2 == 0 && y % 2 == 0)
        {
            base.MakeTile(x, y);
        }

    }

}
