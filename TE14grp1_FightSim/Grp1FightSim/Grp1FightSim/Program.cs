﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grp1FightSim
{
    class Program
    {
        public static List<int> il = new List<int>();

        static void Main(string[] args)
        {
            Console.WriteLine("Skriv namnet på A: ");
            string nameA = Console.ReadLine();

            Fighter fighterA = new Fighter();

            Console.WriteLine("Skriv namnet på B: ");
            string nameB = Console.ReadLine();
            
            Fighter fighterB = new Fighter();

            fighterA.name = nameA;
            fighterB.name = nameB;

            int damage;
            int round = 1;

            while (fighterA.IsAlive() && fighterB.IsAlive())
            {
                Console.WriteLine("--== ROUND " + round + " ==--");
                damage = fighterA.Attack();
                fighterB.Hurt(damage);

                damage = fighterB.Attack();
                fighterA.Hurt(damage);

                Console.WriteLine(fighterA.name + " " + fighterA.GetHp());
                Console.WriteLine(fighterB.name + " " + fighterB.GetHp());
                round++;
            }

            if (fighterA.IsAlive())
            {
                Console.WriteLine("Grattis " + fighterA.name);
            }
            else if (fighterB.IsAlive())
            {
                Console.WriteLine("Grattis " + fighterB.name);
            }
            else
            {
                Console.WriteLine("INGEN VANN! =(");
            }

            Console.ReadLine();

        }
    }
}
