﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanGrp1
{
    class Program
    {


        static void Main(string[] args)
        {

            // Slumpa ett ord

            string[] ordlista = new string[4]
                {"perfekt", "dator", "programmering", "barbapappa"};

            List<char> felGissning = new List<char>();

            int gissningar = 10;

            Random generator = new Random();

            int ordIndex = generator.Next(ordlista.Length);

            string ordet = ordlista[ordIndex];

            // Skapa array med streck

            string[] streck = new string[ordet.Length];

            for (int i = 0; i < streck.Length; i++)
            {
                streck[i] = "_";
            }


            while (gissningar > 0 && string.Join("", streck) != ordet)
            {
                // Skriv ut strecken
                Console.WriteLine(string.Join(" ", streck));

                // Skriv ut de felaktiga gissningarna
                Console.Write("Felaktiga: ");
                Console.WriteLine(string.Join(" ", felGissning));

                // Ber om en bokstav
                Console.Write("Gissa på en bokstav!");
                string gissning = Console.ReadLine();


                // Går igenom ordet och byter ut streck där bokstaven matchar
                bool finnsIOrdet = false;
                
                for (int n = 0; n < ordet.Length; n++)
                {
                    if (ordet[n] == gissning[0])
                    {
                        streck[n] = ordet[n].ToString();
                        finnsIOrdet = true;
                    }
                }

                // Drar av gissningar om bokstaven inte hittades.
                if (!finnsIOrdet)
                {
                    gissningar--;
                    felGissning.Add(gissning[0]);
                }
            }

            

            Console.ReadLine();
        }
    }
}
