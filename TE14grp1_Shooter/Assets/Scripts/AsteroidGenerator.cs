﻿using UnityEngine;
using System.Collections;

public class AsteroidGenerator : MonoBehaviour {

    float timer = 0;

    [SerializeField]
    float timerMax = 2;

    [SerializeField]
    GameObject asteroidPrefab;
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer > timerMax)
        {
            Instantiate(asteroidPrefab);
            timer = 0;
        }


	}
}
