﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ProjectileController : MonoBehaviour {

    [SerializeField]
    float speed = 2f;

    Vector3 movement;



	// Use this for initialization
	void Start () {
        movement = Vector3.up * speed;
    }
	
	// Update is called once per frame
	void Update () {

        transform.Translate(movement * Time.deltaTime);

        if (transform.position.y > 5f)
        {
            Destroy(this.gameObject);
        }

	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);

            

        }
    }
}
