﻿using UnityEngine;
using System.Collections;

public class StarController : MonoBehaviour
{
    Vector3 movement;
    float speed = 1f;

    // Use this for initialization
    void Start()
    {
        speed = Random.Range(2f, 10f);


        movement = Vector3.down * speed;

        transform.position = new Vector3(Random.Range(-4.5f, 4.5f), Random.Range(-4.5f, 4.5f), 0);

        float size = Random.Range(0.1f, 0.2f);

        Vector3 startSize = new Vector3(size, size, 1);

        transform.localScale = startSize;

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(movement * Time.deltaTime);

        if (transform.position.y < -5f)
        {
            Vector3 newPosition = new Vector3(Random.Range(-4.5f, 4.5f), 5, 0);

            transform.position = newPosition;
        }

    }
}
