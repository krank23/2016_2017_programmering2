﻿using UnityEngine;
using System.Collections;

public class StarGenerator : MonoBehaviour
{
    [SerializeField]
    GameObject starPrefab;

    [SerializeField]
    int numberOfStars = 100;

    // Use this for initialization
    void Start()
    {

        for (int i = 0; i < numberOfStars; i++)
        {
            Instantiate(starPrefab);
        }
    }

}
