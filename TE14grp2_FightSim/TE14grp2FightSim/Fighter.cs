﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE14grp2FightSim
{
    class Fighter
    {
        private int hp = 10;
        public string name = "";
        private Random rnd = new Random();

        public int Attack()
        {
            
            return rnd.Next(0, 6);
        }

        public void Hurt(int amount)
        {
            hp -= amount;

            hp = Math.Max(0, hp);
            hp = Math.Min(10, hp);
        }

        public bool IsAlive()
        {
            return hp > 0;

        }

        public int GetHp()
        {
            return hp;
        }
    }
}
