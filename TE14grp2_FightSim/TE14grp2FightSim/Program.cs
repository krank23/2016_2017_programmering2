﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE14grp2FightSim
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Skriv namn på A: ");
            string nameA = Console.ReadLine();
            Fighter fighterA = new Fighter();

            Console.WriteLine("Skriv namn på B: ");
            string nameB = Console.ReadLine();
            Fighter fighterB = new Fighter();

            fighterA.name = nameA;
            fighterB.name = nameB;

            int damage;

            while (fighterA.IsAlive() && fighterB.IsAlive())
            {
                Console.WriteLine("-----~~~~~===== NY ROND =====~~~~~-----");
                damage = fighterA.Attack();
                fighterB.Hurt(damage);

                damage = fighterB.Attack();
                fighterA.Hurt(damage);

                Console.WriteLine(fighterA.name + ": " + fighterA.GetHp());
                Console.WriteLine(fighterB.name + ": " + fighterB.GetHp());
            }

            if (fighterA.IsAlive())
            {
                Console.WriteLine("Grattis " + fighterA.name);
            }
            else if (fighterB.IsAlive())
            {
                Console.WriteLine("Grattis " + fighterB.name);
            }
            else
            {
                Console.WriteLine("Oavgjort!");
            }

            

            Console.ReadLine();
        }
    }
}
