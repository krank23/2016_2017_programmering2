﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanGrp2
{
    class Program
    {
        static void Main(string[] args)
        {

            Random generator = new Random();

            string[] ordlista = new string[3] { "banan", "häst", "gurka" };
            int index = generator.Next(ordlista.Length);
            string ordet = ordlista[index];

            List<char> fel = new List<char>();

            //Console.WriteLine(ordet);

            string[] strecken = new string[ordet.Length];

            for (int i=0; i < ordet.Length; i++)
            {
                strecken[i] = "_";
            }

            while (fel.Count() < 10 && String.Join("",strecken) != ordet )
            {
                Console.WriteLine(String.Join(" ", strecken));ordet.

                string gissning = Console.ReadLine();

                bool harHittats = false;

                for (int n = 0; n < ordet.Length; n++)
                {
                    if (ordet[n] == gissning[0])
                    {
                        strecken[n] = ordet[n].ToString();
                        harHittats = true;
                    }
                }

                if (!harHittats)
                {
                    fel.Add(gissning[0]);
                }

            }
            Console.ReadLine();

        }
    }
}
