﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    float speed = 0.1f;

    [SerializeField]
    GameObject projectilePrefab;

    [SerializeField]
    Transform projectileOrigin;

    float coolDownValue = 0;

    [SerializeField]
    float coolDownMax = .1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        float xMove = Input.GetAxisRaw("Horizontal");
        float yMove = Input.GetAxisRaw("Vertical");

        //Debug.Log(xMove);
        Vector3 movementX = new Vector3(xMove, 0, 0) * speed;
        Vector3 movementY = new Vector3(0, yMove, 0) * speed;

        transform.Translate(movementX + movementY);

        if (transform.position.x > 5 || transform.position.x < -5)
        {
            transform.Translate(-movementX);
        }

        if (transform.position.y > 5 || transform.position.y < -5)
        {
            transform.Translate(-movementY);
        }

        coolDownValue += Time.deltaTime;

        if (Input.GetAxisRaw("Fire1") > 0 && coolDownValue > coolDownMax)
        {
            coolDownValue = 0;
            Instantiate(projectilePrefab, projectileOrigin.position, Quaternion.identity);
        }
        
    }
}
