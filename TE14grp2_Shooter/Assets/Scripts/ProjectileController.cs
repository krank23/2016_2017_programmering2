﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {

    [SerializeField]
    float speed = 3f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 movement = Vector3.up * speed * Time.deltaTime;

        transform.Translate(movement);

        if (transform.position.y > 6)
        {
            Destroy(this.gameObject);
        }

	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
    }
}
