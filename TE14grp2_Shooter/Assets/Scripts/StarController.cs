﻿using UnityEngine;
using System.Collections;

public class StarController : MonoBehaviour {

    float speed = 1;

	// Use this for initialization
	void Start () {
        float x = Random.Range(-5f, 5f);
        float y = Random.Range(-5f, 5f);

        Vector3 startPos = new Vector3(x, y, 0);

        transform.position = startPos;

        speed = Random.Range(3f, 20f);


        float size = Random.Range(0.1f, 0.2f);

        Vector3 startSize = new Vector3(size, size, 0);

        transform.localScale = startSize;

	}
	
	// Update is called once per frame
	void Update () {

        Vector3 movementY = Vector3.down * speed * Time.deltaTime;

        transform.Translate(movementY);

        if (transform.position.y < -5.5f)
        {

            float xPosition = Random.Range(-5f, 5f);

            transform.position = new Vector3(xPosition, 5.5f, 0);
        }

	}
}
