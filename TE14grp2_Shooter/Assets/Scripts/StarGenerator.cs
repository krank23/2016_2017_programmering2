﻿using UnityEngine;
using System.Collections;

public class StarGenerator : MonoBehaviour {

    [SerializeField]
    GameObject starPrefab;

	// Use this for initialization
	void Start () {

        for (int i = 0; i < 100; i++)
        {
            Instantiate(starPrefab);
        }

	}
}
